// MQTT Thing Accessory plugin for Homebridge
// A Homebridge plugin for serveral services, based on homebrige-mqtt-switch and homebridge-mqttlightbulb

'use strict';

var Service, Characteristic;
var mqtt = require("mqtt");
var request = require("request");

function makeThing(log, config) {

    // MQTT message dispatch
    var mqttDispatch = {}; // map of topic to function( topic, message ) to handle

    var logmqtt = config.logMqtt;

    function mqttInit() {
        var clientId = 'mqttthing_' + config.name.replace(/[^\x20-\x7F]/g, "") + '_' + Math.random().toString(16).substr(2, 8);
        var options = {
            keepalive: 10,
            clientId: clientId,
            protocolId: 'MQTT',
            protocolVersion: 4,
            clean: true,
            reconnectPeriod: 1000,
            connectTimeout: 30 * 1000,
            will: {
                topic: 'WillMsg',
                payload: 'Connection Closed abnormally..!',
                qos: 0,
                retain: false
            },
            username: config.username,
            password: config.password,
            rejectUnauthorized: false
        };

        var mqttClient = mqtt.connect(config.url, options);
        mqttClient.on('error', function (err) {
            log('MQTT Error: ' + err);
        });

        mqttClient.on('message', function (topic, message) {
            if (logmqtt) {
                log("Received MQTT: " + topic + " = " + message);
            }
            var handler = mqttDispatch[topic];
            if (handler) {
                handler(topic, message);
            } else {
                log('Warning: No MQTT dispatch handler for topic [' + topic + ']');
            }
        });

        return mqttClient;
    }

    // Initialize MQTT client
    var mqttClient = mqttInit();

    function mqttSubscribe(topic, handler) {
        mqttDispatch[topic] = handler;
        mqttClient.subscribe(topic);
    }

    function mqttPublish(topic, message) {
        if( logmqtt ) {
            log( 'Publishing MQTT: ' + topic + ' = ' + message );
        }
        mqttClient.publish(topic, message.toString());
    }

    var c_mySetContext = '---my-set-context--';

    // The states of our characteristics
    var state = {};

    var timer;

    var waitForOff;

    var waitForStable;

    var buttonOnTime;

    function callAPI(command, callback) {
    		const url = `http://${config.ip}/${command}`;
    		request({
    			url: url,
    			method: "get",
    			timeout: 5000
    		}, function(err, response) {
          if (callback) {
              callback(err);
          }
    		});
	   }

     function setOn(value) {
       unstable(500, function() {
         var hold;

         if (state.targetBrightness == 0) {
           if (value) {
               hold = 0.25;
           } else {
               hold = 0.25;
               waitForOff = null;
               waitForStable = null;
           }
         }

         if (hold) {
           if (timer) {
             clearTimeout(timer);
           }

           callAPI(`settings/relay/1?auto_off=${hold}`, function(err) {
             if (err === null) {
               callAPI("relay/1?turn=on");
             }
           });
         }
       });
     }

     function setBrightness(value, timeout) {
        state.targetBrightness = value;

        unstable(timeout, function() {
          var diff = state.targetBrightness - state.brightness;
          var sign = Math.sign(diff);

          var dimming = function() {
            var hold = Math.max(Math.abs(diff) / 100.0 * config.cycleDuration, config.reverseDuration * 2);
            if ( (state.targetBrightness > 90 && diff > 0) || (state.targetBrightness < 10 && diff < 0 ) ) {
              hold++;
            }

            log(`brightness diff = ${diff}`);
            log(`hold button ${hold}s`);

            callAPI(`settings/relay/1?auto_off=${hold}`, function(err) {
              if (err === null) {
                callAPI("relay/1?turn=on");
              }
            });
          }

          log(`last ramping = ${state.sign}`);
          if (state.sign != 0 && Math.sign(diff) === state.sign) {
            reverse(dimming)
          } else {
            dimming();
          }
        });
     }

     function reverse(callback) {
       log("change direction");
       callAPI(`settings/relay/1?auto_off=${config.reverseDuration}`, function(err) {
         if (err === null) {
           callAPI("relay/1?turn=on", function(err) {
             if (err === null) {
               waitForOff = callback;
             }
           });
         }
       });
     }

     function stopDimming(callback) {
       log("stop Dimming");
       var buttonOff = function() {
         callAPI("relay/1?turn=off", callback);
       }
       var now = new Date().getTime();
       var buttonOnDuration = now - buttonOnTime;
       var wait = config.reverseDuration * 1000 - buttonOnDuration

       if ( wait > 0 ) {
         setTimeout(buttonOff, wait);
       } else {
         buttonOff();
       }
     }

     function unstable(timeout, callback) {
       if (timer) {
         clearTimeout(timer);
       }

       state.stable = false;
       log("unstable");

       if (timeout) {
         timer = setTimeout( function() {
           state.stable = true;
           log("stable");
           if (callback) {
             callback();
           }
           if (waitForStable) {
             waitForStable();
             waitForStable = null;
           }
         }, timeout);
       }
     }

     function updateBrightness(service) {
       if (state.on) {
          state.stableBrightness = state.brightness;
          log("stable brightness: " + state.stableBrightness);
          service.getCharacteristic(Characteristic.Brightness).setValue(state.brightness, undefined, c_mySetContext);
       }
     }

    // Create service
    function createServices() {

        var name = config.name;
        var service = new Service.Lightbulb(name);
        //characteristic_On(service);
        service.getCharacteristic(Characteristic.On)
        .on('get', function (callback) {
            callback(null, state.on);
        })
        .on('set', function (value, callback, context) {
            if (context !== c_mySetContext && value !== state.on) {
              log("set on = " + value);
              setOn(value);
            }
            callback();
        });

        //characteristic_Brightness(service);
        service.getCharacteristic(Characteristic.Brightness)
        .on('get', function (callback) {
            if (state.on) {
                callback(null, state.brightness);
            } else {
                callback(null, state.stableBrightness);
            }
        })
        .on('set', function (value, callback, context) {
            if (context !== c_mySetContext && value !== state.brightness) {
              log("set brightness = " + value);

              if (state.stable) {
                if(Math.abs(value - state.stableBrightness) > 5) {
                  setBrightness(value, 1000);
                }
              } else {
                waitForStable = function() {
                  setBrightness(value, 1000);
                }
              }
            }
            callback();
        });

        mqttSubscribe(config.topics.getPower, function (topic, message) {
            var power = parseFloat(message);
            state.power = power;

            unstable(1000, function() {
              updateBrightness(service);
            });

            log("power: " + power);

            var minPower = config.minPower ? config.minPower : 0.0;
            var maxPower = config.maxPower ? config.maxPower : 100.0;

            var brightness;

            if (power <= minPower) {
              brightness = 0;
              state.sign = 0;
            } else if (power >= maxPower) {
              brightness = 100;
            } else {
              brightness = Math.round(power * 100.0 / maxPower);
            }

            log("brightness: " + brightness);

            var targetDiff = brightness - state.targetBrightness

            if (state.targetBrightness) {
                log("target diff: " + targetDiff);
            }

            if (state.brightness !== brightness) {
                let diff = brightness - state.brightness;

                if (Math.abs(diff) > 1) {
                    state.sign = Math.sign(diff);
                    log("sign: " + state.sign);
                }

                state.brightness = brightness;
            }

            var isOn = power > minPower;

            log("isOn: " + isOn);

            if (isOn && state.targetBrightness) {
              if (brightness >= 100
                || brightness <= 1
                || (state.sign != 0 && Math.abs(targetDiff) <= 2) ) {

                log(`close to target: ${state.targetBrightness}` );
                stopDimming();

              }
            }

            if (state.on !== isOn) {
                state.on = isOn;
                service.getCharacteristic(Characteristic.On).setValue(isOn, undefined, c_mySetContext);
            }
        });

        mqttSubscribe(config.topics.getOn, function (topic, message) {
            if (message == 'on') {
              log("button on");
              buttonOnTime = new Date().getTime();
              unstable();
            } else if (message == 'off') {
              log("button off");
              if (waitForOff) {
                waitForOff();
                waitForOff = null;
              } else {
                state.targetBrightness = 0;
                unstable(1000, function() {
                  updateBrightness(service);
                });
              }
            }
        });

        service.getCharacteristic(Characteristic.Name)
        .on('get', function (callback) {
            callback(null, config.name);
        });

        log("config: " + config);

        return [ service ];
    }

    // The service
    var services = createServices();

    // Our accessory instance
    var thing = {};

    // Return services
    thing.getServices = function () {
        return services;
    };

    return thing;
}

// Homebridge Entry point
module.exports = function (homebridge) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;

    homebridge.registerAccessory("homebridge-shelly2-dimmer", "shelly2-dimmer", makeThing);
}
